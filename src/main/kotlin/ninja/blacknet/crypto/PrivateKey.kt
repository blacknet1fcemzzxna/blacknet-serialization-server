package ninja.blacknet.crypto

import ninja.blacknet.serialization.fromHex
import ninja.blacknet.serialization.toHex

/**
 * Ed25519 private key
 */
class PrivateKey(val bytes: ByteArray) {
    override fun equals(other: Any?): Boolean = (other is PrivateKey) && bytes.contentEquals(other.bytes)
    override fun hashCode(): Int = bytes.contentHashCode()
    override fun toString(): String = bytes.toHex()

    fun toPublicKey(): PublicKey {
        return Ed25519.publicKey(this)
    }

    companion object {
        const val SIZE = 32

        fun fromString(hex: String?): PrivateKey? {
            if (hex == null) return null
            val bytes = fromHex(hex, SIZE) ?: return null
            return PrivateKey(bytes)
        }
    }
}

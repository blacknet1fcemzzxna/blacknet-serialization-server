package ninja.blacknet.crypto

/**
 * Blacknet address
 */
object Address {
    private val HRP = "blacknet".toByteArray(Charsets.US_ASCII)

    fun encode(publicKey: PublicKey): String {
        return Bech32.encode(HRP, publicKey.bytes)
    }

    fun decode(string: String?): PublicKey? {
        if (string == null)
            return null
        val (hrp, data) = Bech32.decode(string) ?: return null
        if (!HRP.contentEquals(hrp))
            return null
        if (data.size != PublicKey.SIZE)
            return null
        return PublicKey(data)
    }
}

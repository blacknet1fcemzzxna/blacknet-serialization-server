package ninja.blacknet.serialization

import kotlinx.serialization.Decoder
import kotlinx.serialization.Encoder
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.json.JsonInput
import kotlinx.serialization.json.JsonOutput
import ninja.blacknet.util.emptyByteArray

/**
 * Serializable [ByteArray]
 */
@Serializable
class SerializableByteArray(
    val array: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        return (other is SerializableByteArray) && array.contentEquals(other.array)
    }

    override fun hashCode(): Int {
        return array.contentHashCode()
    }

    override fun toString(): String {
        return array.toHex()
    }

    @Serializer(forClass = SerializableByteArray::class)
    companion object {
        val EMPTY = SerializableByteArray(emptyByteArray())

        fun fromString(hex: String?): SerializableByteArray? {
            if (hex == null) return null
            val bytes = fromHex(hex) ?: return null
            return SerializableByteArray(bytes)
        }

        override fun deserialize(decoder: Decoder): SerializableByteArray {
            return when (decoder) {
                is BinaryDecoder -> SerializableByteArray(decoder.decodeByteArray())
                is JsonInput -> fromString(decoder.decodeString())!!
                else -> throw RuntimeException("Unsupported decoder")
            }
        }

        override fun serialize(encoder: Encoder, obj: SerializableByteArray) {
            when (encoder) {
                is BinaryEncoder -> encoder.encodeByteArray(obj.array)
                is JsonOutput -> encoder.encodeString(obj.array.toHex())
                else -> throw RuntimeException("Unsupported encoder")
            }
        }
    }
}

package ninja.blacknet.serialization

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.json.JsonElement

object Json {
    private val json = Json(
        JsonConfiguration(
            prettyPrint = false,
            indent = "    "
        )
    )

    fun <T> parse(deserializer: DeserializationStrategy<T>, string: String): T = json.parse(deserializer, string)
    fun parseJson(string: String): JsonElement = json.parseJson(string)
    fun <T> stringify(serializer: SerializationStrategy<T>, obj: T): String = json.stringify(serializer, obj)
    fun <T : Any?> toJson(serializer: SerializationStrategy<T>, obj: T): JsonElement = json.toJson(serializer, obj)
}

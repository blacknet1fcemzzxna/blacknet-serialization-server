package ninja.blacknet.serialization


import kotlinx.io.core.ByteReadPacket
import kotlinx.io.core.IoBuffer
import kotlinx.io.core.readBytes
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.ElementValueDecoder
import kotlinx.serialization.SerialDescriptor
import kotlinx.serialization.internal.EnumDescriptor
import java.nio.ByteBuffer
import kotlin.experimental.and

/**
 * Decoder from the Blacknet Binary Format
 */
class BinaryDecoder(private val input: ByteReadPacket) : ElementValueDecoder() {
    fun <T : Any?> decode(loader: DeserializationStrategy<T>): T {
        val v = loader.deserialize(this)
        val remaining = input.remaining
        if (remaining == 0L) {
            return v
        } else {
            input.release()
            throw RuntimeException("$remaining trailing bytes")
        }
    }

    override fun decodeByte(): Byte = input.readByte()
    override fun decodeShort(): Short = input.readShort()
    override fun decodeInt(): Int = input.readInt()
    override fun decodeLong(): Long = input.readLong()

    override fun decodeNotNullMark(): Boolean = input.readByte() != 0.toByte()
    override fun decodeBoolean(): Boolean = input.readByte() != 0.toByte()
    override fun decodeFloat(): Float = input.readFloat()
    override fun decodeDouble(): Double = input.readDouble()

    override fun decodeString(): String {
        val size = decodeVarInt()
        return String(input.readBytes(size))
    }

    override fun decodeEnum(enumDescription: EnumDescriptor): Int = decodeVarInt()
    override fun decodeCollectionSize(desc: SerialDescriptor): Int = decodeVarInt()

    fun decodeByteArray(): ByteArray {
        val size = decodeVarInt()
        return input.readBytes(size)
    }

    fun decodeFixedByteArray(size: Int): ByteArray {
        return input.readBytes(size)
    }

    fun decodeVarInt(): Int {
        var ret = 0
        var v: Byte
        do {
            v = input.readByte()
            ret = ret shl 7 or (v and 0x7F.toByte()).toInt()
        } while (v and 0x80.toByte() == 0.toByte())
        return ret
    }

    fun decodeVarLong(): Long {
        var ret = 0L
        var v: Byte
        do {
            v = input.readByte()
            ret = ret shl 7 or (v and 0x7F.toByte()).toLong()
        } while (v and 0x80.toByte() == 0.toByte())
        return ret
    }

    companion object {
        fun fromBytes(bytes: ByteArray): BinaryDecoder {
            val buf = IoBuffer(ByteBuffer.wrap(bytes))
            buf.resetForRead()
            val input = ByteReadPacket(buf, IoBuffer.NoPool)
            return BinaryDecoder(input)
        }
    }
}

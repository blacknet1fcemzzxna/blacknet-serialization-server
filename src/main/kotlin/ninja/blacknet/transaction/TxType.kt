package ninja.blacknet.transaction


import kotlinx.serialization.KSerializer

enum class TxType(val type: Byte) {
    Transfer(0),
    Burn(1),
    Lease(2),
    CancelLease(3),
    Bundle(4),
    CreateHTLC(5),
    UnlockHTLC(6),
    RefundHTLC(7),
    SpendHTLC(8),
    CreateMultisig(9),
    SpendMultisig(10),
    WithdrawFromLease(11),
    ClaimHTLC(12),
    Generated(254.toByte()),
    ;

    companion object {
        fun getSerializer(type: Byte): KSerializer<out TxData> {
            return when (type) {
                Transfer.type -> ninja.blacknet.transaction.Transfer.serializer()
                Burn.type -> ninja.blacknet.transaction.Burn.serializer()
                Lease.type -> ninja.blacknet.transaction.Lease.serializer()
                CancelLease.type -> ninja.blacknet.transaction.CancelLease.serializer()
                else -> throw RuntimeException("Unknown transaction type $type")
            }
        }
    }
}

package ninja.blacknet.transaction

import kotlinx.serialization.Serializable
import ninja.blacknet.crypto.Address
import ninja.blacknet.crypto.PublicKey
import ninja.blacknet.serialization.BinaryDecoder
import ninja.blacknet.serialization.BinaryEncoder
import ninja.blacknet.serialization.Json

@Serializable
class CancelLease(
    val amount: Long,
    val to: PublicKey,
    val height: Int
) : TxData {
    override fun getType() = TxType.CancelLease
    override fun involves(publicKey: PublicKey) = to == publicKey
    override fun serialize() = BinaryEncoder.toBytes(serializer(), this)
    override fun toJson() = Json.toJson(Info.serializer(), Info(this))

    companion object {
        fun deserialize(bytes: ByteArray): CancelLease = BinaryDecoder.fromBytes(bytes).decode(serializer())
    }

    @Suppress("unused")
    @Serializable
    class Info(
        val amount: String,
        val to: String,
        val height: Int
    ) {
        constructor(data: CancelLease) : this(
            data.amount.toString(),
            Address.encode(data.to),
            data.height
        )
    }
}

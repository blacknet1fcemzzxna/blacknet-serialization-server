package ninja.blacknet.transaction

import kotlinx.serialization.json.JsonElement
import ninja.blacknet.crypto.PublicKey
import ninja.blacknet.serialization.BinaryDecoder

interface TxData {
    fun getType(): TxType
    fun involves(publicKey: PublicKey): Boolean
    fun serialize(): ByteArray
    fun toJson(): JsonElement

    companion object {
        fun deserialize(type: Byte, bytes: ByteArray): TxData {
            val serializer = TxType.getSerializer(type)
            return BinaryDecoder.fromBytes(bytes).decode(serializer)
        }
    }
}

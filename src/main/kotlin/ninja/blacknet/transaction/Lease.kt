package ninja.blacknet.transaction

import kotlinx.serialization.Serializable
import ninja.blacknet.crypto.Address
import ninja.blacknet.crypto.PublicKey
import ninja.blacknet.serialization.BinaryDecoder
import ninja.blacknet.serialization.BinaryEncoder
import ninja.blacknet.serialization.Json

@Serializable
class Lease(
    val amount: Long,
    val to: PublicKey
) : TxData {
    override fun getType() = TxType.Lease
    override fun involves(publicKey: PublicKey) = to == publicKey
    override fun serialize() = BinaryEncoder.toBytes(serializer(), this)
    override fun toJson() = Json.toJson(Info.serializer(), Info(this))

    companion object {
        fun deserialize(bytes: ByteArray): Lease = BinaryDecoder.fromBytes(bytes).decode(serializer())
    }

    @Suppress("unused")
    @Serializable
    class Info(
        val amount: String,
        val to: String
    ) {
        constructor(data: Lease) : this(
            data.amount.toString(),
            Address.encode(data.to)
        )
    }
}

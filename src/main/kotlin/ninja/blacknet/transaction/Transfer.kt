package ninja.blacknet.transaction


import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import ninja.blacknet.crypto.Address
import ninja.blacknet.crypto.Message
import ninja.blacknet.crypto.PublicKey
import ninja.blacknet.serialization.BinaryEncoder
import ninja.blacknet.serialization.Json

@Serializable
class Transfer(
    val amount: Long,
    val to: PublicKey,
    val message: Message
) : TxData {
    override fun getType() = TxType.Transfer
    override fun involves(publicKey: PublicKey) = to == publicKey
    override fun serialize() = BinaryEncoder.toBytes(serializer(), this)
    override fun toJson() = Json.toJson(Info.serializer(), Info(this))

    @Suppress("unused")
    @Serializable
    class Info(
        val amount: String,
        val to: String,
        val message: JsonElement
    ) {
        constructor(data: Transfer) : this(
            data.amount.toString(),
            Address.encode(data.to),
            data.message.toJson()
        )
    }
}

package ninja.blacknet.transaction

import kotlinx.serialization.Serializable
import ninja.blacknet.crypto.PublicKey
import ninja.blacknet.serialization.BinaryEncoder
import ninja.blacknet.serialization.Json
import ninja.blacknet.serialization.SerializableByteArray
import ninja.blacknet.serialization.toHex

@Serializable
class Burn(
    val amount: Long,
    val message: SerializableByteArray
) : TxData {
    override fun getType() = TxType.Burn
    override fun involves(publicKey: PublicKey) = false
    override fun serialize() = BinaryEncoder.toBytes(serializer(), this)
    override fun toJson() = Json.toJson(Info.serializer(), Info(this))

    @Suppress("unused")
    @Serializable
    class Info(
        val amount: String,
        val message: String
    ) {
        constructor(data: Burn) : this(
            data.amount.toString(),
            data.message.array.toHex()
        )
    }
}

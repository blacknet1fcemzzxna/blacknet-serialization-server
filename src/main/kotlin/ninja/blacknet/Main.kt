package ninja.blacknet

import io.ktor.server.netty.*
import io.ktor.routing.*
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.receiveParameters
import io.ktor.response.*
import io.ktor.server.engine.*
import ninja.blacknet.core.*
import ninja.blacknet.crypto.*
import ninja.blacknet.serialization.SerializableByteArray
import ninja.blacknet.serialization.toHex
import ninja.blacknet.transaction.*

object Main{
    @JvmStatic
    fun main(args: Array<String>) {
        val server = embeddedServer(Netty, port = 8284) {
            routing {
                get("/") {
                    call.respondText("It Works!", ContentType.Text.Plain)
                }

                post("/api/v2/serialize/transfer") {
                    val parameters = call.receiveParameters()
                    val from = Address.decode(parameters["from"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid from")
                    val fee = parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid fee")
                    val amount = parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid amount")
                    val to = Address.decode(parameters["to"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid to")
                    // signed message does not supported currently
                    val message = Message.create(parameters["message"], Message.PLAIN, null, to) ?: return@post call.respond(HttpStatusCode.BadRequest, "Failed to create message")
                    // val referenceChain = parameters["referenceChain"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid reference chain") }
                    val seq = parameters["seq"]?.toIntOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid seq")

                    val data = Transfer(amount, to, message).serialize()
                    val tx = Transaction.create(from, seq, Hash.ZERO, fee, TxType.Transfer.type, data)
                    call.respond(HttpStatusCode.OK, tx.serialize().toHex())
                }

                post("/api/v2/serialize/burn") {
                    val parameters = call.receiveParameters()
                    val from = Address.decode(parameters["from"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid from")
                    val fee = parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid fee")
                    val amount = parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid amount")
                    val message = SerializableByteArray.fromString(parameters["message"].orEmpty()) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid message")
                    // val referenceChain = parameters["referenceChain"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid reference chain") }
                    val seq = parameters["seq"]?.toIntOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid seq")

                    val data = Burn(amount, message).serialize()
                    val tx = Transaction.create(from, seq, Hash.ZERO, fee, TxType.Burn.type, data)
                    call.respond(HttpStatusCode.OK, tx.serialize().toHex())
                }

                post("/api/v2/serialize/lease") {
                    val parameters = call.receiveParameters()
                    val from = Address.decode(parameters["from"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid from")
                    val fee = parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid fee")
                    val amount = parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid amount")
                    val to = Address.decode(parameters["to"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid to")
                    // val referenceChain = parameters["referenceChain"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid reference chain") }
                    val seq = parameters["seq"]?.toIntOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid seq")

                    val data = Lease(amount, to).serialize()
                    val tx = Transaction.create(from, seq, Hash.ZERO, fee, TxType.Lease.type, data)
                    call.respond(HttpStatusCode.OK, tx.serialize().toHex())
                }

                post("/api/v2/serialize/cancellease") {
                    val parameters = call.receiveParameters()
                    val from = Address.decode(parameters["from"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid from")
                    val fee = parameters["fee"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid fee")
                    val amount = parameters["amount"]?.toLongOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid amount")
                    val to = Address.decode(parameters["to"]) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid to")
                    val height = parameters["height"]?.toIntOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid height")
                    // val referenceChain = parameters["referenceChain"]?.let { Hash.fromString(it) ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid reference chain") }
                    val seq = parameters["seq"]?.toIntOrNull() ?: return@post call.respond(HttpStatusCode.BadRequest, "Invalid seq")

                    val data = CancelLease(amount, to, height).serialize()
                    val tx = Transaction.create(from, seq, Hash.ZERO, fee, TxType.CancelLease.type, data)
                    call.respond(HttpStatusCode.OK, tx.serialize().toHex())
                }
            }
        }
        server.start(wait = true)
    }
}